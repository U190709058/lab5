public class GCDLoop {
    public static void main(String[] args) {
        int num1= Integer.parseInt(args[0]);
        int num2 = Integer.parseInt(args[1]);
        int ebob = 1;

        if (num1 > num2) {
            for (int i=num2; i > 1; i--) {
                if (num2 % i == 0 & num1 % i == 0) {
                    ebob = i;
                    break;
                }
            }
        } else {
            for (int i=num1; i > 1; i--) {
                if (num2 % i == 0 & num1 % i == 0) {
                    ebob = i;
                    break;
                }
            }
        }
        System.out.println(ebob + " found!");
    }
}
