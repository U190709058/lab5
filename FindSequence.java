import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Arrays;

public class FindSequence {

	public static void main(String[] args) throws FileNotFoundException{
		int[][] matrix = readMatrix();
		// System.out.println("Before:");
		// printMatrix(matrix);

		boolean found = false;
search: for (int i=0; i< matrix.length; i++) {
			for (int j=0; j < matrix[i].length; j++) {
				if (matrix[i][j] == 0) {
					if (search(matrix, i, j)) {
						found = true;
						break search;
					}
				}
			}
		}
			
		if (found) {
			System.out.println("A sequence is found");
		}
		// System.out.println("After:");
		printMatrix(matrix);

	}

	private static boolean search (int[][] matrix, int row, int col) {
		int[][] founded = { {row, col} };
		String before = "none";

		for (int i = 1; i < matrix.length; i++) {
			boolean found = false;

			// Up
			if (row > 0 && !before.equals("up"))
				if (matrix[row - 1][col] == i) {
					row--;
					found = true;
					before = "down";
				}
			// Right
			if (!found && col > 0 && !before.equals("right"))
				if (matrix[row][col - 1] == i) {
					col--;
					found = true;
					before = "left";
				}
			// Down
			if (!found && row < matrix.length && !before.equals("down"))
				if (matrix[row + 1][col] == i) {
					row++;
					found = true;
					before = "up";
				}
			// Left
			if (!found && col < matrix[row].length && !before.equals("left"))
				if (matrix[row][col + 1] == i) {
					col++;
					found = true;
					before = "right";
				}

			if (!found) return false;

			// Add to the list for reversing
			founded = Arrays.copyOf(founded, founded.length + 1);
			founded[founded.length - 1] = new int[]{row, col};
		}

		// Before returning this success, reverse.
		for (int i = 0; i < founded.length; i++) {
			row = founded[i][0];
			col = founded[i][1];
			// System.out.println(row + " - " + col);
			matrix[row][col] = 9 - i;
		}

		return true;
	}


	private static int[][] readMatrix() throws FileNotFoundException{
		int[][] matrix = new int[10][10];
		File file = new File("matrix.txt");

		try (Scanner sc = new Scanner(file)){

			int i = 0;
			int j = 0;
			while (sc.hasNextLine()) {
				int number = sc.nextInt();
				matrix[i][j] = number;
				if (j == 9)
					i++;
				j = (j + 1) % 10;
				if (i == 10)
					break;
			}
		} catch (FileNotFoundException e) {
			throw e;
		}
		return matrix;
	}
	
	private static void printMatrix(int[][] matrix) {
		for (int[] ints : matrix) {
			for (int anInt : ints) {
				System.out.print(anInt + " ");
			}
			System.out.println();
		}	
	}
}
